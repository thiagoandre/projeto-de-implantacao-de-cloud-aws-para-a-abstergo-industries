# Projeto de Implantação de Cloud AWS para a Abstergo Industries

A empresa farmacêutica Abstergo Industries, um hub de distribuição que se comunica com diversas outras empresas, está buscando implementar uma solução de cloud computing para reduzir custos e melhorar sua eficiência operacional. A empresa atualmente não possui nenhuma infraestrutura de cloud instalada, o que representa uma oportunidade para a implantação de serviços da AWS que possam atender às suas necessidades específicas. Este projeto propõe a implementação de três principais serviços da AWS - Amazon S3, Amazon EC2 e Amazon RDS - com o objetivo de proporcionar à Abstergo Industries uma infraestrutura escalável, segura e de baixo custo.

## 1. Amazon S3 (Simple Storage Service)

__Objetivo:__ Implementar o Amazon S3 para oferecer à Abstergo Industries um armazenamento de dados seguro, durável e altamente escalável.

- __Redução de custos de armazenamento:__ O Amazon S3 permite armazenar grandes volumes de dados a um custo acessível, eliminando a necessidade de investimento em infraestrutura física.
- __Escalabilidade automática:__ A capacidade de armazenamento do S3 se adapta facilmente às necessidades da empresa, evitando gastos desnecessários com recursos subutilizados.
- __Durabilidade e disponibilidade:__ Os dados armazenados no S3 são altamente duráveis e disponíveis, reduzindo os riscos de perda de informações e interrupções no acesso aos dados.

__Foco da Ferramenta:__ Armazenamento de dados seguro, durável e escalável, com redução de custos operacionais relacionados à infraestrutura física de armazenamento.


## 2. Amazon EC2 (Elastic Compute Cloud)

__Objetivo:__ Implementar o Amazon EC2 para fornecer à Abstergo Industries recursos computacionais escaláveis e flexíveis.

- __Eliminação de custos com servidores físicos:__ Com o Amazon EC2, a empresa não precisará mais manter servidores físicos, reduzindo custos de compra, manutenção e energia.
- __Pagamento por uso:__ A Abstergo pagará apenas pelos recursos computacionais que utilizar, evitando gastos com capacidade ociosa.
- __Escalabilidade automática:__ O Amazon EC2 permite escalar os recursos computacionais de acordo com a demanda, garantindo que a empresa tenha sempre a capacidade necessária para operar de forma eficiente.

__Foco da Ferramenta:__ Prover recursos computacionais sob demanda, eliminando a necessidade de investimento em infraestrutura física e reduzindo os custos operacionais relacionados à manutenção de servidores.


## 3. Amazon RDS (Relational Database Service)

__Objetivo:__ Implementar o Amazon RDS para gerenciar os bancos de dados da Abstergo Industries de forma eficiente e escalável.

- __Redução de custos de administração:__ O Amazon RDS automatiza tarefas de administração de banco de dados, como backup, recuperação e escalonamento, reduzindo custos operacionais.
- __Eficiência no uso de recursos:__ O RDS otimiza o uso de recursos de banco de dados, reduzindo os custos com infraestrutura e melhorando o desempenho.
- __Facilidade de escalabilidade:__ O Amazon RDS permite escalar os bancos de dados de forma rápida e fácil, acompanhando o crescimento da empresa sem a necessidade de investimentos adicionais em hardware ou software.

__Foco da Ferramenta:__ Gerenciamento eficiente e escalável de bancos de dados relacionais, reduzindo os custos operacionais e melhorando a disponibilidade e desempenho dos sistemas de banco de dados da empresa.


Com a implementação dessas ferramentas, a Abstergo Industries poderá reduzir significativamente seus custos com infraestrutura, eliminando gastos com servidores físicos, armazenamento local e administração de banco de dados. Além disso, a empresa terá acesso a recursos computacionais e de armazenamento altamente escaláveis, permitindo melhor adaptação às demandas do mercado e maior eficiência operacional.

A implementação dos serviços de cloud computing da AWS - Amazon S3, Amazon EC2 e Amazon RDS - representa um passo importante para a modernização e eficiência operacional da Abstergo Industries. Com a migração para a nuvem, a empresa poderá reduzir significativamente seus custos com infraestrutura, eliminando a necessidade de investimento em servidores físicos, armazenamento local e administração de banco de dados.

Além da redução de custos, a empresa também se beneficiará da escalabilidade e flexibilidade oferecidas pela AWS. Os recursos computacionais e de armazenamento poderão ser facilmente escalados de acordo com a demanda do negócio, garantindo que a Abstergo tenha sempre a capacidade necessária para atender às suas necessidades operacionais.

A implementação desses serviços será realizada de forma gradual, com a migração de cargas de trabalho específicas para a nuvem e a adaptação de processos internos para tirar o máximo proveito das funcionalidades oferecidas pela AWS. A equipe responsável pelo projeto será composta por profissionais especializados em cloud computing e AWS, que irão garantir uma implementação segura, eficiente e alinhada com as necessidades da Abstergo Industries.

Em resumo, a implementação da cloud AWS permitirá à Abstergo Industries tornar suas operações mais ágeis, eficientes e econômicas, mantendo-se competitiva em um mercado em constante evolução.
